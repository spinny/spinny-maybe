require 'spinny/maybe/version'
require 'singleton'

# FIXME: Can't use contracts because it has a Maybe (and probably other things).

module Maybe
  class << self
    def from(obj)
      obj.nil? ? Nothing : Just[obj]
    end

    alias_method :[], :from
  end

  # 
  def fmap(fn=nil, &block)
    raise NotImplementedError
  end

  # <relrod> f returns a new Maybe, but instead of getting Maybe[Maybe[B]] you just get Maybe[B]
  def flatmap(fn=nil, &block)
    raise NotImplementedError
  end
  alias_method :bind, :flatmap

  # <relrod> another one is ap (aka (<*>) or "applicative map")
  # <relrod> x.ap(f)   in this case x is a maybe *containing* a function
  def applicative_map(fn=nil, &block)
    raise NotImplementedError
  end
  alias_method :ap, :applicative_map

  # fold/cata/maybe/whateveryouwanttocallit
  # b -> (a -> b) -> Maybe a -> b
  def maybe(default, fn=nil, &block)
    fn ||= block

    fmap(&fn).or(default)
  end
  alias_method :fold, :maybe

  def or(default)
    raise NotImplementedError
  end

  # join, sequence, ...?
end

class Just
  include Maybe

  def initialize(value)
    @value = value
  end

  def self.[](value)
    self.new(value)
  end

  def fmap(fn=nil, &block)
    fn ||= block

    Just[fn.call(@value)]
  end

  #Contract Or[Proc, nil] => Maybe
  def flatmap(fn=nil, &block)
    fn ||= block

    ret = fn.call(@value)

    unless ret.is_a?(Maybe)
      raise TypeError, "expected flatmap to return a Maybe, got a #{ret.class}"
    end

    ret
  end

#  def applicative_map(fn=nil, &block)
#    fn ||= block

#   #...
#  end

  def or(_)
    @value
  end

  def inspect
    "#<Just #{@value.inspect}>"
  end
end

class NothingClass
  include Maybe, Singleton

  def fmap(fn=nil, &block)
    self
  end

  def flatmap(fn=nil, &block)
    self
  end

  def applicative_map(fn=nil, &block)
    self
  end

  def or(default)
    default
  end

  def inspect
    "#<Nothing>"
  end
end

Nothing = NothingClass.instance

