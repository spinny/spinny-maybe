# Spinny::Maybe

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'spinny-maybe'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install spinny-maybe

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it ( https://gitlab.com/spinny/spinny-maybe/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
